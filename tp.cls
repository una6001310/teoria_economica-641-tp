% Identificación de la clase
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{tp}[2014/08/16 Clase para TP de la UNA]

\LoadClassWithOptions{amsbook}
\RequirePackage{microtype}
\RequirePackage[top=2.5cm,right=2.5cm,bottom=2.5cm,left=2.5cm]{geometry}
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{amsfonts}
\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage{amsthm}
\RequirePackage{caption}
\RequirePackage[OT1]{fontenc}
\RequirePackage[pdftex]{graphicx}
\RequirePackage[pdftex]{hyperref}
\RequirePackage[shortlabels]{enumitem}
\RequirePackage{subfig}
\RequirePackage{booktabs}
\RequirePackage{xspace}{}
\RequirePackage{tikz}
\RequirePackage[none]{hyphenat}

\makeatletter
\def\@roman#1{\romannumeral #1}
\makeatother

\renewcommand*{\baselinestretch}{1.1}
\parindent=0cm
\parskip=3mm

\allowdisplaybreaks

\newenvironment{sola}{%
    \vspace{5mm}
    \noindent\textbf{Resolución:}\newline}%
{\begin{flushright}\dotfill\ensuremath{\blacksquare}\end{flushright}%
}

\newenvironment{sol}{%
    \section{Resolución}}%
{\begin{flushright}\dotfill\ensuremath{\blacksquare}\end{flushright}%
}

% Ejemplo \objs{3}{a & b & c}{&&}
% El primer argumento es el numero de celdas vacías a crear
% El segudo argumento es un texto con las cabeceras de las celdas vacías, separadas por '&'
% El tercer argumento es una cadena con la misma cantidad de '&' que el segundo argumento.
% (1 menos que #1)
\newcommand{\objs}[3]{
    \begin{center}
        \begin{tabular}{|l|*{#1}{c|}}
            \hline
            Núm. Objetivo & #2 \\ \hline
            0=NL; 1=L     & #3 \\ \hline
        \end{tabular}
    \end{center}
}

\newcommand{\criterio}[1]{\hfill{#1}\\[3mm]}

%% \sectionlr
\newcommand{\sectionlr}[2]{
    \section{#1}
    \criterio{#2}
}


%% \portada
\newcommand{\portada}{%
    \begin{titlepage}
        \sffamily
        \noindent
        \includegraphics[width=1.3cm,bb=0 0 1024 1024,keepaspectratio=true]{logo.png}\hspace{0.4ex}
        \parbox[b]{50ex}{\bfseries\small UNIVERSIDAD NACIONAL ABIERTA\\
            VICERRECTORADO ACADÉMICO\\
            ÁREA INGENIERÍA}
        \vspace{0.5cm}
        \begin{center}
            \textbf{\titulo}
        \end{center}
        \vspace{0.5cm}
        \addtolength{\arrayrulewidth}{0.5mm}
        \begin{tabular}{|p{14cm}}
            \noindent
            ASIGNATURA: \textbf{\asignatura}                                         \\
            \\
            CÓDIGO: \textbf{\codasig}                                                \\
            \\
            FECHA DE ENTREGA AL ESTUDIANTE: \textbf{\fentrega}                       \\
            \\
            FECHA DE DEVOLUCIÓN POR PARTE DEL ESTUDIANTE: \textbf{\fdevolucion}      \\
            \\
            NOMBRE DEL ESTUDIANTE:                                                   \\
            \textbf{\nombre}                                                         \\
            \\
            CÉDULA DE IDENTIDAD:                                                     \\
            \textbf{\cedula}                                                         \\
            \\
            CORREO ELECTRÓNICO:                                                      \\
            \textbf{\correo}                                                         \\
            \\
            TELEFONO:                                                                \\
            \textbf{\telf}                                                           \\
            \\
            CENTRO LOCAL: \textbf{Trujillo}\hspace{5cm}CARRERA: \textbf{\codcarrera} \\
            \\
            NÚMERO DE ORIGINALES: \textbf{\numorig}.                                 \\
            \\
            FIRMA DEL ESTUDIANTE:                                                    \\
            \\
            \\
            \\
            LAPSO: \textbf{\semestre}                                                \\
            \hline
        \end{tabular}
        \begin{center}
            RESULTADOS DE CORRECCIÓN:
            \resultado
        \end{center}
    \end{titlepage}
}
